<?php

declare(strict_types=1);

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

final class ImageProcessedNotification extends Notification
{
    use Queueable;

    public $photo;
    public $user;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = $photo->user()->first();
    }

    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Dear {$this->user->name()},")
            ->line("Photos have been successfully uploaded and processed.")
            ->line("Here are links to the images:")
            ->line($this->photo->photo100x100())
            ->line($this->photo->photo150x150())
            ->line($this->photo->photo250x250())
            ->line("Thanks!");
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => 'success',
            'photo_100_100' => Storage::url($this->photo->photo100x100()),
            'photo_150_150' => Storage::url($this->photo->photo150x150()),
            'photo_250_250' => Storage::url($this->photo->photo250x250())
        ]);
    }
}
