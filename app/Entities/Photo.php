<?php

declare(strict_types=1);

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Photo
 * @package App\Entities
 * @property int $id
 * @property string $original_photo
 * @property string photo_100_100
 * @property string photo_150_150
 * @property string photo_250_250
 * @property string $status
 * @property int $user_id
 */
final class Photo extends Model
{
    protected $fillable = [
        'original_photo',
        'photo_100_100',
        'photo_150_150',
        'photo_250_250',
        'status',
        'user_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function originalPhoto(): string
    {
        return $this->original_photo;
    }

    public function photo100x100(): string
    {
        return $this->photo_100_100;
    }

    public function photo150x150(): string
    {
        return $this->photo_150_150;
    }

    public function photo250x250(): string
    {
        return $this->photo_250_250;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function userId(): int
    {
        return $this->user_id;
    }
}
