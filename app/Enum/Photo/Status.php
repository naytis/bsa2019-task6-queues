<?php

declare(strict_types=1);

namespace App\Enum\Photo;

final class Status
{
    public const UPLOADED = 'UPLOADED';
    public const PROCESSING = 'PROCESSING';
    public const SUCCESS = 'SUCCESS';
    public const FAIL = 'FAIL';
}
