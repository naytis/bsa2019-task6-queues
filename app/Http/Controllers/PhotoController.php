<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Entities\Photo;
use App\Jobs\CropJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

final class PhotoController extends Controller
{
    public function store(Request $request)
    {
        $userId = Auth::id();
        $photoName = $request->file('photo')->getClientOriginalName();
        $photoPath = $request->file('photo')->storeAs("images/{$userId}", $photoName);

        $photo = Photo::firstOrCreate([
            'original_photo' => $photoPath,
            'user_id' => $userId
        ]);

        CropJob::dispatch($photo);

        return response()->json(['success' => true], 200);
    }
}
