<?php

declare(strict_types=1);

namespace App\Services;

use App\Services\Contracts\PhotoService as IPhotoService;
use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use App\Services\Contracts\AuthService;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

final class PhotoService implements IPhotoService
{
    private $authService;
    private $fileSystem;

    public function __construct(Filesystem $fileSystem, AuthService $authService)
    {
        $this->fileSystem = $fileSystem;
        $this->authService = $authService;
    }

    public function crop(string $pathToPhoto, int $width, int $height): string
    {
        $croppedImage = Image::make(
            Storage::path($pathToPhoto)
        )->crop($width, $height);

        $photoInfo = pathinfo($pathToPhoto);

        $filePath = $photoInfo['dirname']
            . '/'
            . $photoInfo['filename']
            . "{$width}x{$height}"
            . '.'
            . $croppedImage->extension;

        $this->fileSystem->put(
            $filePath,
            $croppedImage->stream()
        );

        return $filePath;
    }
}
