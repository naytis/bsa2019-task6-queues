<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Entities\Photo;
use App\Enum\Photo\Status;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

final class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $photo;
    public $tries = 3;

    public function __construct(Photo $photo)
    {
        $this->user = $photo->user()->first();
        $this->photo = $photo;
    }

    public function handle(PhotoService $photoService)
    {
        $photo = $this->photo;
        $photo->status = Status::PROCESSING;
        $photo->save();

        $photo->photo_100_100 = $photoService->crop($photo->originalPhoto(), 100, 100);
        $photo->photo_150_150  = $photoService->crop($photo->originalPhoto(), 150, 150);
        $photo->photo_250_250  = $photoService->crop($photo->originalPhoto(), 250, 250);

        $photo->status = Status::SUCCESS;
        $photo->save();

        $this->user->notify(new ImageProcessedNotification($photo));
    }

    public function failed(\Exception $exception)
    {
        $this->photo->status = Status::FAIL;
        $this->photo->save();
        $this->user->notify(new ImageProcessingFailedNotification());
    }
}
